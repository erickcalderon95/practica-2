package ventana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.JTextArea;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setTitle("Alta de Socios");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Calderon\\Desktop\\16x16-icon.gif"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 605, 558);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(new Color(102, 205, 170));
		menuBar.setForeground(Color.LIGHT_GRAY);
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenu mnNuevo = new JMenu("Nuevo");
		mnArchivo.add(mnNuevo);
		
		JMenu mnProyecto = new JMenu("Proyecto");
		mnNuevo.add(mnProyecto);
		
		JMenu mnCargarArchivo = new JMenu("Cargar Archivo");
		mnArchivo.add(mnCargarArchivo);
		
		JMenu mnGuardar = new JMenu("Guardar");
		mnArchivo.add(mnGuardar);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenu mnCopiar = new JMenu("Copiar");
		mnEditar.add(mnCopiar);
		
		JMenu mnPegar = new JMenu("Pegar");
		mnEditar.add(mnPegar);
		
		JMenu mnCortar = new JMenu("Cortar");
		mnEditar.add(mnCortar);
		
		JMenu mnNewMenu = new JMenu("Ayuda");
		menuBar.add(mnNewMenu);
		
		JMenu mnGuiaDeUso = new JMenu("Guia de uso");
		mnNewMenu.add(mnGuiaDeUso);
		
		JMenu mnQa = new JMenu("Q&A");
		mnNewMenu.add(mnQa);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 191, 255));
		contentPane.setForeground(Color.WHITE);
		contentPane.setToolTipText("Dar de Alta");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 47, 56, 16);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(12, 76, 56, 16);
		contentPane.add(lblApellido);
		
		JLabel lblNmeroDeSocio = new JLabel("N\u00FAmero de Socio");
		lblNmeroDeSocio.setBounds(12, 105, 106, 16);
		contentPane.add(lblNmeroDeSocio);
		
		textField = new JTextField();
		textField.setBounds(126, 44, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(126, 73, 116, 22);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(126, 102, 116, 22);
		contentPane.add(textField_2);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setToolTipText("Alta");
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Zaragoza", "Madird", "Barcelona", "Valencia ", "Bilbao", "Sevilla"}));
		comboBox.setBounds(396, 44, 83, 22);
		contentPane.add(comboBox);
		
		JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setBounds(291, 44, 56, 16);
		contentPane.add(lblCiudad);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(291, 101, 90, 25);
		contentPane.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(396, 101, 90, 25);
		contentPane.add(rdbtnMujer);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1549753200000L), new Date(1549753200000L), null, Calendar.DAY_OF_YEAR));
		spinner.setBounds(126, 148, 97, 16);
		contentPane.add(spinner);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(51, 207, 285, 194);
		contentPane.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		textArea.setBackground(new Color(230, 230, 250));
		scrollPane.setViewportView(textArea);
		
		JSlider slider = new JSlider();
		scrollPane.setColumnHeaderView(slider);
		
		JButton btnComentar = new JButton("Comentar");
		btnComentar.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/descarga.png")));
		btnComentar.setBounds(51, 399, 129, 25);
		contentPane.add(btnComentar);
		
		JButton btnBorarTexto = new JButton("Borar texto");
		btnBorarTexto.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/descarga (1).png")));
		btnBorarTexto.setBounds(207, 399, 129, 25);
		contentPane.add(btnBorarTexto);
		
		JLabel lblDaDeAlta = new JLabel("D\u00EDa de Alta");
		lblDaDeAlta.setBounds(12, 148, 83, 16);
		contentPane.add(lblDaDeAlta);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setForeground(new Color(255, 215, 0));
		progressBar.setOpaque(true);
		progressBar.setToolTipText("");
		progressBar.setValue(25);
		progressBar.setBounds(51, 437, 296, 35);
		contentPane.add(progressBar);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(-6, 0, 259, 18);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("Alta");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cancelar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Editar");
		toolBar.add(btnNewButton_2);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Premiun");
		chckbxNewCheckBox.setBackground(new Color(255, 215, 0));
		chckbxNewCheckBox.setBounds(366, 207, 113, 25);
		contentPane.add(chckbxNewCheckBox);
		
		JCheckBox chckbxStandar = new JCheckBox("Estandar");
		chckbxStandar.setBackground(new Color(192, 192, 192));
		chckbxStandar.setBounds(366, 251, 113, 25);
		contentPane.add(chckbxStandar);
		
		JCheckBox chckbxSimple = new JCheckBox("Simple");
		chckbxSimple.setBackground(new Color(184, 134, 11));
		chckbxSimple.setBounds(366, 296, 113, 25);
		contentPane.add(chckbxSimple);
	}
}
