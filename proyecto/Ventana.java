package proyecto;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JSlider;
import javax.swing.JProgressBar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ButtonGroup;
import javax.swing.JTextPane;
import javax.swing.JCheckBox;
import java.awt.Toolkit;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.ImageIcon;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField txtComentario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Calderon\\Desktop\\iconos\\16x16-icon.gif"));
		setTitle("Alta Clientes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 597, 525);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(new Color(105, 105, 105));
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);
		
		JMenu mnNewMenu_6 = new JMenu("Abrir");
		mnNewMenu.add(mnNewMenu_6);
		
		JMenu mnArchivoNuevo = new JMenu("Archivo Nuevo");
		mnNewMenu_6.add(mnArchivoNuevo);
		
		JMenu mnNewMenu_5 = new JMenu("Cerrar");
		mnNewMenu.add(mnNewMenu_5);
		
		JMenu mnNewMenu_3 = new JMenu("Guardar");
		mnNewMenu.add(mnNewMenu_3);
		
		JMenu mnNewMenu_1 = new JMenu("Editar");
		menuBar.add(mnNewMenu_1);
		
		JMenu mnNewMenu_7 = new JMenu("Copiar");
		mnNewMenu_1.add(mnNewMenu_7);
		
		JMenu mnNewMenu_4 = new JMenu("Pegar");
		mnNewMenu_1.add(mnNewMenu_4);
		
		JMenu mnNewMenu_8 = new JMenu("Cortar");
		mnNewMenu_1.add(mnNewMenu_8);
		
		JMenu mnNewMenu_2 = new JMenu("Ayuda");
		menuBar.add(mnNewMenu_2);
		
		JMenu mnNewMenu_9 = new JMenu("Guia de uso");
		mnNewMenu_2.add(mnNewMenu_9);
		
		JMenu mnNewMenu_10 = new JMenu("Q&A");
		mnNewMenu_2.add(mnNewMenu_10);
		
		JMenuBar menuBar_1 = new JMenuBar();
		mnNewMenu_2.add(menuBar_1);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(100, 149, 237));
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 307, 18);
		contentPane.add(toolBar);
		
		JButton btnNewButton = new JButton("Alta");
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Baja");
		toolBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Cancelar");
		toolBar.add(btnNewButton_2);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(12, 47, 56, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Apellido");
		lblNewLabel_1.setBounds(12, 76, 56, 16);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("N\u00FAmero de Socio");
		lblNewLabel_2.setBounds(12, 105, 127, 16);
		contentPane.add(lblNewLabel_2);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Mujer");
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(428, 96, 127, 25);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Hombre");
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setBounds(293, 96, 127, 25);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 205, 301, 186);
		contentPane.add(scrollPane);
		
		JSlider slider = new JSlider();
		scrollPane.setColumnHeaderView(slider);
		
		JTextPane textPane = new JTextPane();
		textPane.setBackground(new Color(255, 222, 173));
		scrollPane.setViewportView(textPane);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setValue(25);
		progressBar.setBounds(28, 414, 307, 25);
		contentPane.add(progressBar);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Zaragoza", "Barcelona", "Madrid", "Sevilla", "Valencia ", "Bilbao"}));
		comboBox.setBounds(377, 43, 133, 25);
		contentPane.add(comboBox);
		
		JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setBounds(292, 47, 56, 16);
		contentPane.add(lblCiudad);
		
		textField_1 = new JTextField();
		textField_1.setBounds(124, 44, 116, 22);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(124, 73, 116, 22);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(124, 102, 116, 22);
		contentPane.add(textField_3);
		
		txtComentario = new JTextField();
		txtComentario.setText("Comentario");
		txtComentario.setBounds(28, 181, 116, 22);
		contentPane.add(txtComentario);
		txtComentario.setColumns(10);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Premiun");
		chckbxNewCheckBox.setBackground(new Color(255, 215, 0));
		chckbxNewCheckBox.setBounds(397, 214, 113, 25);
		contentPane.add(chckbxNewCheckBox);
		
		JCheckBox chckbxEstandar = new JCheckBox("Estandar");
		chckbxEstandar.setBackground(new Color(192, 192, 192));
		chckbxEstandar.setBounds(397, 244, 113, 25);
		contentPane.add(chckbxEstandar);
		
		JCheckBox chckbxSimple = new JCheckBox("Simple");
		chckbxSimple.setBackground(new Color(184, 134, 11));
		chckbxSimple.setBounds(397, 278, 113, 25);
		contentPane.add(chckbxSimple);
		
		JLabel lblFechaAlta = new JLabel("Fecha Alta");
		lblFechaAlta.setBounds(12, 152, 96, 16);
		contentPane.add(lblFechaAlta);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1549753200000L), new Date(1549753200000L), null, Calendar.DAY_OF_YEAR));
		spinner.setBounds(124, 146, 116, 22);
		contentPane.add(spinner);
		
		JButton btnContinuar = new JButton("Continuar");
		btnContinuar.setIcon(new ImageIcon("C:\\Users\\Calderon\\Desktop\\iconos\\descarga (1).png"));
		btnContinuar.setForeground(new Color(255, 255, 255));
		btnContinuar.setBackground(new Color(34, 139, 34));
		btnContinuar.setBounds(411, 342, 129, 25);
		contentPane.add(btnContinuar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon("C:\\Users\\Calderon\\Desktop\\iconos\\descarga.png"));
		btnCancelar.setForeground(new Color(255, 255, 255));
		btnCancelar.setBackground(new Color(255, 0, 0));
		btnCancelar.setBounds(413, 380, 127, 25);
		contentPane.add(btnCancelar);
	}
}
